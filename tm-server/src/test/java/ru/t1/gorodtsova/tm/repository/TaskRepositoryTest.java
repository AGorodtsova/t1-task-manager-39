package ru.t1.gorodtsova.tm.repository;

import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {
/*
    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final Connection connection = connectionService.getConnection();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connection);

    @After
    @SneakyThrows
    public void dropConnection() {
        connection.rollback();
        connection.close();
    }

    @Test
    public void add() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.findAll().get(0));
        Assert.assertEquals(USER1.getId(), taskRepository.findAll().get(0).getUserId());
    }

    @Test
    public void addAll() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskRepository.findAll(USER1.getId()));
        Assert.assertNotEquals(USER1_TASK_LIST, taskRepository.findAll(USER2.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.findOneById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertNotEquals(USER2_TASK1, taskRepository.findOneById(USER1.getId(), USER2_TASK1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.removeOneById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(taskRepository.findAll().contains(USER1_TASK1));
        Assert.assertTrue(taskRepository.findAll().contains(USER2_TASK1));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.removeOne(USER1.getId(), USER1_TASK1));
        Assert.assertFalse(taskRepository.findAll().contains(USER1_TASK1));
        Assert.assertTrue(taskRepository.findAll().contains(USER2_TASK1));
    }

    @Test
    public void removeAllByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskRepository.findAll());
        taskRepository.removeAll(USER2.getId());
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        taskRepository.removeAll(USER1.getId());
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER2_TASK1);
        taskRepository.removeAll(USER1.getId());
        Assert.assertFalse(taskRepository.findAll().isEmpty());
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        Assert.assertTrue(taskRepository.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskRepository.existsById(USER2_TASK1.getId()));
    }

    @Test
    public void createTaskName() {
        @NotNull final Task task = taskRepository.create(USER1.getId(), "test task");
        Assert.assertEquals(task, taskRepository.findOneById(task.getId()));
        Assert.assertEquals("test task", task.getName());
        Assert.assertEquals(USER1.getId(), task.getUserId());
    }

    @Test
    public void createProjectNameDescription() {
        @NotNull final Task task = taskRepository.create(USER1.getId(), "test task", "test description");
        Assert.assertEquals(task, taskRepository.findOneById(task.getId()));
        Assert.assertEquals("test task", task.getName());
        Assert.assertEquals("test description", task.getDescription());
        Assert.assertEquals(USER1.getId(), task.getUserId());
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(USER2_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskRepository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
    }*/

}
