package ru.t1.gorodtsova.tm.repository;

import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class UserRepositoryTest {
/*
    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final Connection connection = connectionService.getConnection();

    @NotNull
    private final IUserRepository userRepository = new UserRepository(connection);

    @After
    @SneakyThrows
    public void dropConnection() {
        connection.rollback();
        connection.close();
    }

    @Test
    public void add() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertEquals(USER1, userRepository.findAll().get(0));
    }

    @Test
    public void addAll() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER_LIST, userRepository.findAll());
    }

    @Test
    public void set() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER_LIST, userRepository.findAll());
        userRepository.set(USER_LIST2);
        Assert.assertEquals(USER_LIST2, userRepository.findAll());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST1);
        Assert.assertEquals(USER_LIST1, userRepository.findAll());
        Assert.assertNotEquals(USER_LIST2, userRepository.findAll());
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        userRepository.add(USER2);
        Assert.assertEquals(USER1, userRepository.findOneById(USER1.getId()));
        Assert.assertNotEquals(USER2, userRepository.findOneById(USER1.getId()));
        Assert.assertNull(userRepository.findOneById(ADMIN1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        userRepository.add(USER2);
        Assert.assertEquals(USER1, userRepository.removeOneById(USER1.getId()));
        Assert.assertFalse(userRepository.findAll().contains(USER1));
        Assert.assertTrue(userRepository.findAll().contains(USER2));
        Assert.assertThrows(ModelNotFoundException.class, () -> userRepository.removeOneById(USER1.getId()));
    }

    @Test
    public void removeOne() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        userRepository.add(USER2);
        Assert.assertEquals(USER1, userRepository.removeOne(USER1));
        Assert.assertFalse(userRepository.findAll().contains(USER1));
        Assert.assertTrue(userRepository.findAll().contains(USER2));
    }

    @Test
    public void removeAll() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER_LIST, userRepository.findAll());
        userRepository.removeAll();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeAllCollection() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER_LIST, userRepository.findAll());
        userRepository.removeAll(USER_LIST1);
        Assert.assertFalse(userRepository.findAll().contains(USER2));
        Assert.assertTrue(userRepository.findAll().contains(ADMIN1));
    }

    @Test
    public void existsById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertTrue(userRepository.existsById(USER1.getId()));
        Assert.assertFalse(userRepository.existsById(USER2.getId()));
    }

    @Test
    public void findByLogin() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertEquals(USER1, userRepository.findByLogin(USER1.getLogin()));
        Assert.assertNotEquals(USER1, userRepository.findByLogin(USER2.getLogin()));
        @NotNull final User user = new User();
        user.setLogin("login");
        Assert.assertNull(userRepository.findByLogin(user.getLogin()));
    }

    @Test
    public void findByEmail() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertEquals(USER1, userRepository.findByEmail(USER1.getEmail()));
        Assert.assertNotEquals(USER1, userRepository.findByEmail(USER2.getEmail()));
        @NotNull final User user = new User();
        user.setEmail("user@company.ru");
        Assert.assertNull(userRepository.findByEmail(user.getEmail()));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertTrue(userRepository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userRepository.isLoginExist(USER2.getLogin()));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertTrue(userRepository.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(userRepository.isEmailExist(USER2.getEmail()));
    }*/

}
