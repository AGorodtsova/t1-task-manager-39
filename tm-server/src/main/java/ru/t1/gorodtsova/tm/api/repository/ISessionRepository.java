package ru.t1.gorodtsova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Session;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, created, role, user_id) " +
            "VALUES(#{id}, #{date}, #{role}, #{userId});")
    void add(@NotNull Session session);

    @Delete("DELETE FROM tm_session WHERE id = #{id};")
    void removeOneById(@NotNull String id);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    Session findOneById(@NotNull String id);

}
