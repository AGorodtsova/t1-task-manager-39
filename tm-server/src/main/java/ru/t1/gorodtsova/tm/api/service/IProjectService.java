package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.enumerated.ProjectSort;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@Nullable Project model);

    @NotNull
    Collection<Project> add(@NotNull Collection<Project> models);

    void set(@NotNull Collection<Project> projects);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    List<Project> findAll();

    @Nullable
    List<Project> findAll(@Nullable String userId);

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    void removeAll();

    void removeAll(@Nullable String userId);

    @Nullable
    Project removeOne(@Nullable String userId, @Nullable Project project);

    @Nullable
    Project removeOneById(@Nullable String id);

    @NotNull
    Project removeOneById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    int getSize();

    int getSize(@Nullable String userId);

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

}
