package ru.t1.gorodtsova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.component.Bootstrap;

public final class Application {

    public static void main(@Nullable String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}