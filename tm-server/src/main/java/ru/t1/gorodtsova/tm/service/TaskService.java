package ru.t1.gorodtsova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.enumerated.TaskSort;
import ru.t1.gorodtsova.tm.exception.entity.TaskNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.*;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private String getSortType(@Nullable final TaskSort sort) {
        if (sort == TaskSort.BY_CREATED) return "created";
        if (sort == TaskSort.BY_STATUS) return "status";
        else return "name";
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.addTask(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task.setUserId(userId);
            repository.addTask(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Task> add(@Nullable Collection<Task> tasks) {
        if (tasks == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks.forEach(repository::addTask);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Task> set(@Nullable Collection<Task> tasks) {
        if (tasks == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAllTasks();
            tasks.forEach(repository::addTask);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllTasks();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllTasksByUser(userId);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId, @Nullable final TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllTasksByUserWithOrder(userId, getSortType(sort));
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllTasksByProjectId(userId, projectId);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findTaskByIdByUser(userId, id);
            return task;
        }
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAllTasks();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAllTasksByUser(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeOneTaskByIdByUser(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.updateTask(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.updateTask(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Override
    @SneakyThrows
    public int getSize() {
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.getTasksSize();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.getTasksSizeByUser(userId);
        }
    }

}
