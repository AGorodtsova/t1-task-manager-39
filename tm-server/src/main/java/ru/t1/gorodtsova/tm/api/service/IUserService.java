package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    User add(@Nullable User user);

    @NotNull
    Collection<User> add(@Nullable Collection<User> users);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    Collection<User> set(@Nullable Collection<User> users);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @NotNull
    User findByLogin(@Nullable String login);

    @NotNull
    User findByEmail(@Nullable String email);

    void removeAll();

    @NotNull
    User removeOne(@Nullable User user);

    @NotNull
    User removeOneById(@Nullable String id);

    @NotNull
    User removeByLogin(@Nullable String login);

    User setPassword(String id, String password);

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
