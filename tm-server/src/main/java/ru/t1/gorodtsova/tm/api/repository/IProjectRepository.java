package ru.t1.gorodtsova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, created, name, descrptn, status, user_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId});")
    void addProject(@NotNull Project project);

    @Select("SELECT * FROM tm_project;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    @Nullable List<Project> findAllProjects();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    @Nullable List<Project> findAllProjectsByUser(@NotNull String userId);

    @Select("SELECT * FROM tm_project ORDER BY #{orderBy};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    @Nullable List<Project> findAllProjectsWithOrder(@NotNull String orderBy);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY #{orderBy};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    @Nullable List<Project> findAllProjectsByUserWithOrder(@Param("userId") @NotNull String userId, @Param("orderBy") @NotNull String orderBy);

    @Select("SELECT * FROM tm_project WHERE id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    @Nullable Project findProjectById(@NotNull String id);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    @Nullable Project findProjectByIdByUserId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("TRUNCATE TABLE tm_project CASCADE;")
    void removeAll();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId};")
    void removeAllByUser(@NotNull String userId);

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId};")
    void removeOneProject(@NotNull String userId, @NotNull Project project);

    @Delete("DELETE FROM tm_project WHERE id = #{Id};")
    void removeOneProjectById(@NotNull String Id);

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId};")
    void removeOneProjectByIdByUserId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Update("UPDATE tm_project SET name = #{name}, descrptn = #{description}, " +
            "STATUS = #{status} WHERE id = #{id} AND user_id = #{userId};")
    void updateProject(@NotNull Project project);

    @Select("SELECT COUNT(1) = 1 FROM tm_project WHERE id = #{id};")
    boolean existsById(@NotNull String id);

    @Select("SELECT COUNT(1) = 1 FROM tm_project WHERE id = #{id} AND user_id = #{userId};")
    boolean existsByIdUserId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM tm_project;")
    int getProjectsSize();

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId};")
    int getProjectsSizeByUserId(@NotNull String userId);

}
