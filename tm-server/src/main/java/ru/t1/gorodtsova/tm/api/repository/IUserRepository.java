package ru.t1.gorodtsova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password, first_name, last_name, middle_name, " +
            "email, role, locked) VALUES (#{id}, #{login}, #{passwordHash}, #{firstName}, " +
            "#{lastName}, #{middleName}, #{email}, #{role}, #{locked})")
    void addUser(@NotNull final User user);

    @NotNull
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    List<User> findAllUsers();

    @Nullable
    @Select("SELECT * FROM tm_user WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    User findUserById(@NotNull String id);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    User findUserByLogin(@NotNull String login);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    User findUserByEmail(@NotNull String email);

    @Delete("TRUNCATE TABLE tm_user")
    void removeAllUsers();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOneUser(@NotNull User user);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeUserById(@NotNull String id);

    @Update("UPDATE tm_user SET login = #{login}, password = #{passwordHash}, email = #{email}, " +
            "first_name = #{firstName}, last_name = #{lastName}, middle_name = #{middleName}, " +
            "role = #{role}, locked = #{locked} WHERE id = #{id}")
    void updateUser(@NotNull User user);

    @Select("SELECT COUNT(1) FROM tm_user")
    int getUsersSize();

    @Update("UPDATE tm_user SET password = #{password} WHERE id = #{id};")
    void setPassword(@Param("id") @NotNull String id, @Param("password") @NotNull String password);

    @Update("UPDATE tm_user SET locked = true WHERE id = #{id};")
    void lockUserById(@NotNull String id);

    @Update("UPDATE tm_user SET locked = false WHERE id = #{id};")
    void unlockUserById(@NotNull String id);

}
