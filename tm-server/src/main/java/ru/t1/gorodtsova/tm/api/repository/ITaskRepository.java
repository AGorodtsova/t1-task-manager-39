package ru.t1.gorodtsova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, created, name, descrptn, status, user_id, project_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})")
    void addTask(@NotNull Task task);

    @Nullable
    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<Task> findAllTasks();

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<Task> findAllTasksByUser(@NotNull String userId);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<Task> findAllTasksByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY #{orderBy}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<Task> findAllTasksByUserWithOrder(@Param("userId") @NotNull String userId, @Param("orderBy") @NotNull String orderBy);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE ID = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "description", column = "descrptn")
    })
    Task findTaskByIdByUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("TRUNCATE TABLE tm_task CASCADE")
    void removeAllTasks();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void removeAllTasksByUser(@NotNull String userId);

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void removeOneTaskById(@NotNull String id);

    @Delete("DELETE FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    void removeOneTaskByIdByUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Update("UPDATE tm_task SET name = #{name}, description = #{description}, status = #{status}, " +
            "project_id = #{projectId} WHERE id = #{id}")
    void updateTask(@NotNull Task task);

    @Select("SELECT COUNT(1) FROM tm_task")
    int getTasksSize();

    @Select("SELECT COUNT(1) FROM tm_task WHERE user_id = #{userId}")
    int getTasksSizeByUser(@NotNull String userId);

    @Update("UPDATE tm_task SET project_id = #{projectId} WHERE id = #{taskId};")
    void bindTaskToProject(@Param("projectId") @NotNull String projectId, @Param("taskId") @NotNull String taskId);

    @Update("UPDATE tm_task SET project_id = null WHERE id = #{taskId};")
    void unbindTaskFromProject(@NotNull String taskId);

}
