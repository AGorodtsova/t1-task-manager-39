package ru.t1.gorodtsova.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String DESCRIPTION = "Show developer info";

    @NotNull
    private final String NAME = "about";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: " + getPropertyService().getAuthorName());
        System.out.println("email: " + getPropertyService().getAuthorEmail());
    }

}
