package ru.t1.gorodtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.gorodtsova.tm.api.endpoint.IUserEndpoint;
import ru.t1.gorodtsova.tm.dto.request.user.*;
import ru.t1.gorodtsova.tm.dto.response.user.*;
import ru.t1.gorodtsova.tm.marker.IntegrationCategory;
import ru.t1.gorodtsova.tm.model.User;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void init() {
        @Nullable final UserLoginResponse loginTestResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        userToken = loginTestResponse.getToken();
        @Nullable final UserLoginResponse loginAdminResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        adminToken = loginAdminResponse.getToken();
    }

    @After
    public void tearDown() {
        @NotNull final UserLogoutRequest adminLogoutRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(adminLogoutRequest);
        @NotNull final UserLogoutRequest testLogoutRequest = new UserLogoutRequest(userToken);
        authEndpoint.logout(testLogoutRequest);
    }

    @Test
    public void lockUser() {
        @NotNull final UserLockRequest userLockRequest = new UserLockRequest(adminToken, "test");
        @NotNull final UserLockResponse userLockResponse = userEndpoint.lockUser(userLockRequest);
        Assert.assertNotNull(userLockResponse);
        Assert.assertTrue(userLockResponse.getSuccess());
        @NotNull final UserUnlockRequest userUnlockRequest = new UserUnlockRequest(adminToken, "test");
        userEndpoint.unlockUser(userUnlockRequest);

        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(userToken, "test"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(null, "test"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(adminToken, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(adminToken, "wrongLogin"))
        );
    }

    @Test
    public void unlockUser() {
        @Nullable final UserLockResponse userLockResponse = userEndpoint.lockUser(
                new UserLockRequest(adminToken, "test")
        );
        Assert.assertNotNull(userLockResponse);
        @Nullable final UserUnlockResponse userUnlockResponse = userEndpoint.unlockUser(
                new UserUnlockRequest(adminToken, "test")
        );
        Assert.assertNotNull(userUnlockResponse);
        Assert.assertTrue(userUnlockResponse.getSuccess());

        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(userToken, "test"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(null, "test"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(adminToken, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(adminToken, "wrongLogin"))
        );
    }

    @Test
    public void registryUser() {
        @Nullable final UserRegistryRequest userRegistryRequest =
                new UserRegistryRequest(adminToken, "newLogin", "newPassword", "newEmail");
        @Nullable final UserRegistryResponse userRegistryResponse = userEndpoint.registryUser(userRegistryRequest);
        Assert.assertNotNull(userRegistryResponse);
        Assert.assertNotNull(userRegistryResponse.getUser());
        Assert.assertEquals("newLogin", userRegistryResponse.getUser().getLogin());

        @Nullable final UserRemoveRequest userRemoveRequest =
                new UserRemoveRequest(adminToken, userRegistryResponse.getUser().getLogin());
        userEndpoint.removeUser(userRemoveRequest);

        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(null, "newLogin", "newPassword", "newEmail"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(adminToken, "test", "newPassword", "newEmail"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(adminToken, "newLogin", null, "newEmail"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(adminToken, "newLogin", "newPassword", null))
        );
    }

    @Test
    public void removeUser() {
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest(userToken, "newLogin"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest(null, "newLogin"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest(adminToken, null))
        );

        @Nullable final UserRegistryRequest userRegistryRequest =
                new UserRegistryRequest(adminToken, "newLogin", "newPassword", "newEmail");
        userEndpoint.registryUser(userRegistryRequest);

        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("newLogin", "newPassword")
        );
        Assert.assertTrue(loginResponse.getSuccess());

        @NotNull final UserRemoveRequest userRemoveRequest = new UserRemoveRequest(adminToken, "newLogin");
        Assert.assertNotNull(userEndpoint.removeUser(userRemoveRequest));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("newLogin", "newPassword"))
        );
    }

    @Test
    public void updateProfileUser() {
        @NotNull final UserProfileRequest userProfileRequest = new UserProfileRequest(userToken);
        @NotNull final UserProfileResponse userProfileResponse = userEndpoint.viewProfileUser(userProfileRequest);
        Assert.assertNotNull(userProfileResponse.getUser());
        @NotNull User oldUser = userProfileResponse.getUser();
        @Nullable final String oldFirstName = oldUser.getFirstName();
        @Nullable final String oldMiddleName = oldUser.getMiddleName();
        @Nullable final String oldLastName = oldUser.getLastName();

        @Nullable final UserUpdateProfileResponse userUpdateProfileResponse = userEndpoint.updateProfileUser(
                new UserUpdateProfileRequest(userToken, "first", "last", "middle")
        );
        Assert.assertNotNull(userUpdateProfileResponse.getUser());
        @NotNull User user = userUpdateProfileResponse.getUser();
        Assert.assertEquals("first", user.getFirstName());
        Assert.assertEquals("last", user.getLastName());
        Assert.assertEquals("middle", user.getMiddleName());

        userEndpoint.updateProfileUser(
                new UserUpdateProfileRequest(userToken, oldFirstName, oldLastName, oldMiddleName)
        );

        Assert.assertThrows(Exception.class, () -> userEndpoint.updateProfileUser(new UserUpdateProfileRequest()));
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.updateProfileUser(
                        new UserUpdateProfileRequest(null, "first", "last", "middle")
                )
        );
    }

    @Test
    public void viewProfileUser() {
        @Nullable final UserProfileRequest userProfileRequest = new UserProfileRequest(userToken);
        @Nullable final UserProfileResponse userProfileResponse = userEndpoint.viewProfileUser(userProfileRequest);
        Assert.assertNotNull(userProfileResponse.getUser());
        Assert.assertEquals("test", userProfileResponse.getUser().getLogin());

        Assert.assertThrows(Exception.class, () -> userEndpoint.viewProfileUser(new UserProfileRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.viewProfileUser(new UserProfileRequest(null)));
    }

    @Test
    public void changePassword() {
        @Nullable final UserProfileRequest userProfileRequest = new UserProfileRequest(userToken);
        @Nullable final UserProfileResponse userProfileResponse = userEndpoint.viewProfileUser(userProfileRequest);
        Assert.assertNotNull(userProfileResponse.getUser());
        @Nullable final String oldPassword = userProfileResponse.getUser().getPasswordHash();

        @Nullable final UserChangePasswordRequest changePasswordRequest =
                new UserChangePasswordRequest(userToken, "newPassword");
        @Nullable final UserChangePasswordResponse changePasswordResponse =
                userEndpoint.changePassword(changePasswordRequest);
        Assert.assertNotNull(changePasswordResponse);
        Assert.assertNotNull(changePasswordResponse.getUser());
        Assert.assertNotEquals(oldPassword, changePasswordResponse.getUser().getPasswordHash());

        changePasswordRequest.setPassword("test");
        @Nullable final UserChangePasswordResponse undoResponse = userEndpoint.changePassword(changePasswordRequest);
        Assert.assertNotNull(undoResponse);
        Assert.assertNotNull(undoResponse.getUser());

        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.changePassword(new UserChangePasswordRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.changePassword(new UserChangePasswordRequest(null, "pass"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.changePassword(new UserChangePasswordRequest(userToken, null))
        );
    }

}