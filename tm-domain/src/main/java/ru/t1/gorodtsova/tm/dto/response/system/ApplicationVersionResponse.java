package ru.t1.gorodtsova.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import ru.t1.gorodtsova.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ApplicationVersionResponse extends AbstractResponse {

    private String version;

}
